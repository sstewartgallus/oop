/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OOP_H
#define OOP_H

#include <stdint.h>

#define OOP_ARRAY_SIZE(...) (sizeof __VA_ARGS__ / sizeof __VA_ARGS__[0U])

enum {
	OOP_CLASS_ABI_VERSION_0
};
#define OOP_CLASS_ABI_VERSION_LATEST OOP_CLASS_ABI_VERSION_0

struct oop_class;

struct oop_class_dict_entry {
	uint64_t oop_low;
	uint64_t oop_high;
	void (*oop_method)(void);
};

struct oop_object_header {
	struct oop_class const *const*__class;
	uint32_t __refcount;
	char __padding[4U];
};

#define OOP_OBJECT_HEADER_STATIC_INITIALIZER(CLASS) {CLASS, 0U, {0}}

static inline void oop_object_header_init(struct oop_object_header *__header,
					  struct oop_class const *const*__class,
					  _Bool __refcounted)
{
	__header->__class = __class;
	__header->__refcount = __refcounted;
}

struct oop_class {
	struct oop_object_header oop_header;
	uint64_t oop_version;
	char __contents[];
};

struct __oop_inline_pair {
	struct oop_class const *__class;
	void (*__method)(void);
};

struct oop_class_inline_cache {
	struct __oop_inline_pair __pairs[4U];
	unsigned char __counter;

	char __padding[63U];
};

struct oop_class const *oop_class_class;

void (*oop_class_lookup_method(struct oop_class_inline_cache *__cache,
			       struct oop_class const *__class, uint64_t __low,
			       uint64_t __high))(void);

#define oop_invoke(OBJ, METHOD, ...) ({					\
			static __thread struct oop_class_inline_cache __inline_cache = {{{0, 0}}, 0, {0}}; \
			(METHOD##_TYPE*)oop_class_lookup_method(&__inline_cache, *(OBJ)->oop_header.__class, METHOD); \
		})((void*)OBJ, ## __VA_ARGS__)

struct oop_object {
	struct oop_object_header oop_header;
	char __contents[];
};

#define oop_object_dispose 0X367AC3FA51214171, 0X86D7525E03942328
typedef void oop_object_dispose_TYPE(struct oop_object *);

#define oop_object_to_string 0XC4D5237BD1304CB2, 0XA687E8801D6770F1
typedef int oop_object_to_string_TYPE(struct oop_object *, char**);

static inline void oop_object_get(struct oop_object_header *__header)
{
	/* This does not acquire or release anything if the object is
	 * a reference counted object and does not acquire or release
	 * anything if the object is not reference counted. */
	if (0U == __atomic_load_n(&__header->__refcount, __ATOMIC_RELAXED))
		return;

	/* It is totally okay for reads and writes to move before the
	 * reference has been incremented because acquiring another
	 * count in the first place requires a valid object.
	 */

	__atomic_fetch_add(&__header->__refcount, 1U, __ATOMIC_RELAXED);
}
#define oop_object_get(...) oop_object_get(&(__VA_ARGS__)->oop_header)

static inline void oop_object_put(struct oop_object_header *__header)
{
	/* This does not acquire or release anything if the object is
	 * a reference counted object and does not acquire or release
	 * anything if the object is not reference counted. */
	if (0U == __atomic_load_n(&__header->__refcount, __ATOMIC_RELAXED))
		return;

	/* Prevent reads and writes moving after the object has been
	 * released and might be disposed of. */

	__atomic_thread_fence(__ATOMIC_RELEASE);

	if (1U == __atomic_fetch_sub(&__header->__refcount, 1U, __ATOMIC_ACQ_REL)) {
		/* Prevent reads and writes moving before the object
		 * reference count has been decremented to zero and
		 * the object is releasable. */

		__atomic_thread_fence(__ATOMIC_ACQUIRE);

		oop_invoke((struct oop_object*)__header, oop_object_dispose);
	}
	/* No memory barrier should be needed here because NOBODY
	 * should be reading and writing to an obect after they have
	 * released it. If they DO read and write to it then they
	 * should have an extra acquire and it should be fine to move
	 * reads and writes back earlier. */
}
#define oop_object_put(...) oop_object_put(&(__VA_ARGS__)->oop_header)

#define OOP_CLASS_DEFINE(CLASS, ...)					\
	static struct {							\
		struct oop_object_header oop_header;			\
		uint64_t oop_version;					\
		size_t oop_size;					\
		struct oop_class_dict_entry oop_pairs[OOP_ARRAY_SIZE((struct oop_class_dict_entry[]){ __VA_ARGS__ })]; \
	} const CLASS = {						\
		OOP_OBJECT_HEADER_STATIC_INITIALIZER(&oop_class_class),	\
		OOP_CLASS_ABI_VERSION_LATEST,				\
		OOP_ARRAY_SIZE((struct oop_class_dict_entry[]){ __VA_ARGS__}), \
		{							\
			__VA_ARGS__					\
		}							\
	}

#endif /* OOP_H */
