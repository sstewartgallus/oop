/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "oop.h"

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct direction {
	struct oop_object_header oop_header;
	char __contents[];
};

struct oop_class const *const direction_class;

struct direction const *const direction_north;
struct direction const *const direction_south;
struct direction const *const direction_west;
struct direction const *const direction_east;

struct shape {
	struct oop_object_header oop_header;
	char __contents[];
};

#define shape_area 0XF1485F4BE34D4E98, 0XA8F29A4B7D4B1B8B
typedef unsigned shape_area_TYPE(struct shape *);

struct circle {
	struct oop_object_header oop_header;
	char __contents[];
};

struct oop_class const *circle_class;

#define circle_radius 0XBAE4533D125F4C22, 0XA21BEE1BD3BD3F3D
typedef unsigned circle_radius_TYPE(struct circle *);

#define circle_radius_set 0XE647FDF61BB94BD7U, 0XA5D74EBB91473DF3U
typedef void circle_radius_set_TYPE(struct circle *, unsigned);

struct circle *circle_new(unsigned radius);

#define ARRAY_SIZE(X) (sizeof X / sizeof X[0U])

#define invoke oop_invoke

int main(void)
{
	int errnum;

	char *str;
	{
		char *xx;
		errnum = invoke(direction_north, oop_object_to_string, &xx);
		if (errnum != 0) {
			errno = errnum;
			perror("oop_object_to_string");
			return EXIT_FAILURE;
		}
		str = xx;
	}
	fprintf(stderr, "direction: %s\n", str);
	free(str);

	struct circle *circles[20000U];

	for (size_t ii = 0U; ii < ARRAY_SIZE(circles); ++ii) {
		struct circle *circle = circle_new(20U);
		if (NULL == circle) {
			perror("circle_new");
			return EXIT_FAILURE;
		}
		circles[ii] = circle;
	}

	oop_object_get(circles[0U]);
	oop_object_put(circles[0U]);

	unsigned long radius_sum = 0U;
	unsigned long area_sum = 0U;
	for (size_t ii = 0U; ii < ARRAY_SIZE(circles); ++ii) {
		struct circle *circle = circles[ii];

		radius_sum += invoke(circle, circle_radius);
		area_sum += invoke(circle, shape_area);
	}

	fprintf(stderr, "radius sum: %lu area sum: %lu\n", radius_sum,
		area_sum);

	for (size_t ii = 0U; ii < ARRAY_SIZE(circles); ++ii)
		oop_object_put(circles[ii]);
}

struct impl_direction {
	struct oop_object_header oop_header;
};

static oop_object_to_string_TYPE impl_direction_to_string;

OOP_CLASS_DEFINE(direction_class_impl,
		 {oop_object_to_string, (void(*)(void))impl_direction_to_string});

struct impl_direction const direction_impls[] = {
	{OOP_OBJECT_HEADER_STATIC_INITIALIZER(&direction_class)},
	{OOP_OBJECT_HEADER_STATIC_INITIALIZER(&direction_class)},
	{OOP_OBJECT_HEADER_STATIC_INITIALIZER(&direction_class)},
	{OOP_OBJECT_HEADER_STATIC_INITIALIZER(&direction_class)}};

struct oop_class const *const direction_class = (void const*)&direction_class_impl;

struct direction const *const direction_north = (void const*)&direction_impls[0U];
struct direction const *const direction_south = (void const*)&direction_impls[1U];
struct direction const *const direction_west = (void const*)&direction_impls[2U];
struct direction const *const direction_east = (void const*)&direction_impls[3U];

static int impl_direction_to_string(struct oop_object *direction, char **strp)
{
	size_t ii = ((struct impl_direction const*)direction) - direction_impls;
	static char const *const names[] = {
		"north",
		"south",
		"west",
		"east"
	};
	char *str = strdup(names[ii]);
	if (NULL == str)
		return errno;

	*strp = str;
	return 0;
}

struct impl_circle {
	struct oop_object_header oop_header;
	unsigned radius;
};

static oop_object_dispose_TYPE impl_circle_dispose;
static oop_object_to_string_TYPE impl_circle_to_string;
static shape_area_TYPE impl_circle_area;
static circle_radius_TYPE impl_circle_radius;
static circle_radius_set_TYPE impl_circle_radius_set;

OOP_CLASS_DEFINE(circle_class_impl,
		 {oop_object_dispose, (void(*)(void))impl_circle_dispose},
		 {oop_object_to_string, (void(*)(void))impl_circle_to_string},
		 {shape_area, (void(*)(void))impl_circle_area},
		 {circle_radius, (void(*)(void))impl_circle_radius},
		 {circle_radius_set, (void(*)(void))impl_circle_radius_set});

struct oop_class const *circle_class = (void const*)&circle_class_impl;

struct circle *circle_new(unsigned radius)
{
	struct impl_circle *circle = malloc(sizeof *circle);
	if (NULL == circle)
		return NULL;

	oop_object_header_init(&circle->oop_header, &circle_class, true);
	circle->radius = radius;

	return (void*)circle;
}

static void impl_circle_dispose(struct oop_object *circle)
{
	free((struct impl_circle*)circle);
}

static int impl_circle_to_string(struct oop_object *circle, char **strp)
{
	char *str = strdup("circle");
	if (NULL == str)
		return errno;

	*strp = str;
	return 0;
}

static unsigned impl_circle_area(struct shape *circle)
{
	unsigned radius = ((struct impl_circle*)circle)->radius;
	return (22U * radius * radius) / 7U;
}

static unsigned impl_circle_radius(struct circle *circle)
{
	return ((struct impl_circle*)circle)->radius;
}

static void impl_circle_radius_set(struct circle *circle, unsigned radius)
{
	((struct impl_circle*)circle)->radius = radius;
}
