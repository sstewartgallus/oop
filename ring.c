/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define _GNU_SOURCE

#include "oop.h"

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define ARRAY_SIZE(X) (sizeof X / sizeof X[0U])

#define invoke oop_invoke


struct thrd {
	struct oop_object_header oop_header;
	char __contents[];
};

static int thrd_new(struct thrd **thrdp, size_t id);

#define thrd_ring 0X80D5C8DEC491410A, 0X82A048C71E543E08
typedef void thrd_ring_TYPE(struct thrd *, size_t n);

#define thrd_link 0XD77A03A319F94503, 0X9B3EFCB1CD4B4227
typedef void thrd_link_TYPE(struct thrd *, struct thrd *);


static struct thrd *thrds[503U];

int main(void)
{
	int errnum;
	for (size_t ii = 0U; ii < ARRAY_SIZE(thrds); ++ii) {
		struct thrd *thrd;
		{
			struct thrd *xx;
			errnum = thrd_new(&xx, ii);
			if (errnum != 0) {
				errno = errnum;
				perror("thrd_new");
				return EXIT_FAILURE;
			}
			thrd = xx;
		}
		thrds[ii] = thrd;
	}

	for (size_t ii = 0U; ii < ARRAY_SIZE(thrds); ++ii)
		invoke(thrds[ii], thrd_link, thrds[(ii + 1U) % ARRAY_SIZE(thrds)]);

	invoke(thrds[0U], thrd_ring, 1000U);

	for (size_t ii = 0U; ii < ARRAY_SIZE(thrds); ++ii)
		oop_object_put(thrds[ii]);
}

struct impl_thrd {
	struct oop_object_header oop_header;
	struct thrd *next;
	size_t id;
};

static oop_object_dispose_TYPE impl_thrd_dispose;
static oop_object_to_string_TYPE impl_thrd_to_string;
static thrd_ring_TYPE impl_thrd_ring;
static thrd_link_TYPE impl_thrd_link;

OOP_CLASS_DEFINE(thrd_class_impl,
		 {oop_object_dispose, (void(*)(void))impl_thrd_dispose},
		 {oop_object_to_string, (void(*)(void))impl_thrd_to_string},
		 {thrd_ring, (void(*)(void))impl_thrd_ring},
		 {thrd_link, (void(*)(void))impl_thrd_link});

static struct oop_class const *const thrd_class = (void const*)&thrd_class_impl;

static int thrd_new(struct thrd **thrdp, size_t id)
{
	struct impl_thrd *thrd = malloc(sizeof *thrd);
	if (NULL == thrd)
		return errno;

	oop_object_header_init(&thrd->oop_header, &thrd_class, true);
	thrd->id = id;
	thrd->next = NULL;

	*thrdp = (void*)thrd;
	return 0;
}

static void impl_thrd_dispose(struct oop_object *thrd)
{
	free((struct impl_thrd*)thrd);
}

static int impl_thrd_to_string(struct oop_object *thrd, char **strp)
{
	struct impl_thrd *impl = (void*)thrd;

	size_t id = impl->id;

	return asprintf(strp, "thrd(%zu)", impl->id) < 0 ? errno : 0;
}

static void impl_thrd_ring(struct thrd *thrd, size_t n)
{
	struct impl_thrd *impl = (void*)thrd;

	struct thrd *next = impl->next;
	size_t id = impl->id;

	if (0U == n) {
		int errnum;

		char *str;
		{
			char *xx;
			errnum = invoke(thrd, oop_object_to_string, &xx);
			if (errnum != 0) {
				errno = errnum;
				perror("oop_object_to_string");
				exit(EXIT_FAILURE);
			}
			str = xx;
		}
		fprintf(stderr, "%s\n", str);
		free(str);
		return;
	}

	invoke(next, thrd_ring, n - 1U);
}

static void impl_thrd_link(struct thrd *thrd, struct thrd*next)
{
	struct impl_thrd *impl = (void*)thrd;
	impl->next = next;
}
